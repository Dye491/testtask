<?php
/**
 * Created by PhpStorm.
 * User: yuri
 * Date: 27.09.18
 * Time: 14:31
 */

namespace app\filters;


use app\models\User;
use yii\filters\AccessRule;

class UpdateSelfRule extends AccessRule
{
    public function allows($action, $user, $request)
    {
        $result = parent::allows($action, $user, $request);
        if ($result !== true)
            return $result;

        $id = $user->getId();
        if ($action->controller->id == 'user' && isset($request->queryParams['id'])
            && $request->queryParams['id'] == $id) {
            return $this->allow ? true : false;
        }

        return false;
    }
}