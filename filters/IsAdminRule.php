<?php

namespace app\filters;


use app\models\User;
use yii\filters\AccessRule;

class IsAdminRule extends AccessRule
{
    public function allows($action, $user, $request)
    {
        $result = parent::allows($action, $user, $request);
        if ($result !== true) return $result;

        if (($userModel = User::findIdentity($user->getId())) && $userModel->is_admin == true)
            return $this->allow ? true : false;

        return false;
    }
}