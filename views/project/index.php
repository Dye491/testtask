<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $users array */

$this->title = Yii::t('app', 'Projects');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box project-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="box-header">
        <?= Html::a(Yii::t('app', 'Create Project'), ['create'],
            ['class' => 'btn btn-success pull-right']) ?>
    </p>

    <?= GridView::widget([
        'layout' => '<div class="box-body no-padding table-responsive">{items}{pager}</div>',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'headerOptions' => ['style' => 'width: 7%;'],
            ],
            'name',
            [
                'attribute' => 'user_id',
                'content' => function ($model) {
                    return Html::a($model->user->fio, ['/user/view', 'id' => $model->user_id],
                        ['data-pjax' => 0, 'title' => Yii::t('app', 'View')]);
                },
                'filter' => $users,
            ],
            'cost',
            [
                'attribute' => 'start_date',
                'filter' => \yii\jui\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'start_date',
                    'dateFormat' => 'php:d.m.Y',
                    'options' => ['class' => 'form-control'],
                ]),
                'format' => ['date', 'php:d.m.Y'],
            ],
            [
                'attribute' => 'end_date',
                'filter' => \yii\jui\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'end_date',
                    'dateFormat' => 'php:d.m.Y',
                    'options' => ['class' => 'form-control'],
                ]),
                'format' => ['date', 'php:d.m.Y'],
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['style' => 'width: 10%;'],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
