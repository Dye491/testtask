<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Project */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Projects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box project-view">

    <p class="box-header">
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger pull-right',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id],
            ['class' => 'btn btn-primary pull-right', 'style' => 'margin-right: 5px;']) ?>
    </p>

    <div class="box-body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                [
                    'attribute' => 'user_id',
                    'value' => function ($model) {
                        return $model->user->fio;
                    },
                ],
                'name',
                'cost',
                [
                    'attribute' => 'start_date',
                    'format' => ['date', 'php:d.m.Y'],
                ],
                [
                    'attribute' => 'end_date',
                    'format' => ['date', 'php:d.m.Y'],
                ],
            ],
        ]) ?>
    </div>

</div>
