<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box user-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p class="box-header">
        <?= Html::a(Yii::t('app', 'Create User'), ['create'], ['class' => 'btn btn-success pull-right']) ?>
    </p>

    <?= GridView::widget([
        'layout' => '<div class="box-body no-padding table-responsive">{items}{pager}</div>',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'headerOptions' => ['style' => 'width: 10%;'],
            ],
            'fio',
            'username',
            [
                'attribute' => 'is_admin',
                'filter' => [
                    0 => Yii::t('app', 'Yes'),
                    1 => Yii::t('app', 'No'),
                ],
                'value' => function ($model) {
                    return $model->is_admin ? Yii::t('app', 'Yes') : Yii::t('app', 'No');
                },
            ],
            [
                'label' => Yii::t('app', 'Projects'),
                'content' => function ($model) {
                    $count = count($model->projects);
                    return $count ? Html::a($count, ['/project', 'ProjectSearch[user_id]' => $model->id],
                        ['data-pjax' => 0]) : Yii::t('app', 'No');
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['style' => 'width: 10%;'],
                'visibleButtons' => [
                    'delete' => function ($model) {
                        return ($count = count($model->projects)) ? false : true;
                    },
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
