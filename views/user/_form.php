<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\UserEditForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserEditForm */
/* @var $user app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box-body user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>

    <?php if ($model->scenario == \app\models\UserEditForm::SCENARIO_CREATE): ?>

        <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?php endif; ?>

    <?= $form->field($model, 'password')
        ->passwordInput(['maxlength' => true])->label(Yii::t('app', 'Password')) ?>

    <?= $form->field($model, 'password_repeat')
        ->passwordInput(['maxlength' => true]) ?>

    <?php
    if ($model->scenario == UserEditForm::SCENARIO_CREATE ||
        $user->id != Yii::$app->user->id || $user->is_admin)
        echo $form->field($model, 'is_admin')->checkbox();
    ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Cancel'), ['index'], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
