<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">APP</span><span class="logo-lg">' . Yii::t('app', Yii::$app->name)
        . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
                <li>
                    <?php
                    if (!Yii::$app->user->getIsGuest())
                        echo Html::a(Yii::t('app', 'Logout(' . Yii::$app->user->identity->fio . ')'),
                            ['site/logout'], ['data-method' => 'post']);
                    else
                        echo Html::a(Yii::t('app', 'Login'), ['site/login']);
                    ?>
                </li>
            </ul>
        </div>
    </nav>
</header>
