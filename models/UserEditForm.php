<?php

namespace app\models;


use yii\base\Model;

class UserEditForm extends Model
{
    const SCENARIO_CREATE = 'create';

    public
        $fio,
        $username,
        $password,
        $password_repeat,
        $is_admin;

    public function rules()
    {
        return [
            [['fio',], 'required'],
            [['username'], 'required', 'on' => self::SCENARIO_CREATE],
            [['password_repeat'], 'required', 'on' => [self::SCENARIO_CREATE]],
            [['fio', 'username', 'password', 'password_repeat'], 'string'],
            [['username'], 'unique', 'targetAttribute' => 'username', 'targetClass' => User::class,
                'on' => self::SCENARIO_CREATE],
            [['is_admin'], 'boolean'],
            [['password'], 'compare'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'fio' => \Yii::t('app', 'Fio'),
            'username' => \Yii::t('app', 'Username'),
            'password' => \Yii::t('app', 'Password'),
            'password_repeat' => \Yii::t('app', 'Repeat Password'),
            'is_admin' => \Yii::t('app', 'Is Admin'),
        ];
    }

}