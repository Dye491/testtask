<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Project;

/**
 * ProjectSearch represents the model behind the search form of `app\models\Project`.
 */
class ProjectSearch extends Project
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            [['name', 'start_date', 'end_date'], 'safe'],
            [['cost'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Project::find()->with('user');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'cost' => $this->cost,
        ]);

        if (!empty($this->start_date)) {
            $this->start_date = (new \DateTime($this->start_date))->format('Y-m-d');
            $query->andFilterWhere(['start_date' => $this->start_date]);
        }

        if (!empty($this->end_date)) {
            $this->end_date = (new \DateTime($this->end_date))->format('Y-m-d');
            $query->andFilterWhere(['end_date' => $this->end_date]);
        }


        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
