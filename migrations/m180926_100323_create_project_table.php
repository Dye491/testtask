<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project`.
 */
class m180926_100323_create_project_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('project', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'cost' => $this->decimal(18, 2),
            'start_date' => $this->date(),
            'end_date' => $this->date(),
        ]);

        $this->addForeignKey('fk_project_user', 'project', 'user_id', 'user', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('project');
    }
}
