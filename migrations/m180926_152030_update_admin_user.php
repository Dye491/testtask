<?php

use yii\db\Migration;

/**
 * Class m180926_152030_update_admin_user
 */
class m180926_152030_update_admin_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->update('user', ['is_admin' => true], ['username' => 'admin']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->update('user', ['is_admin' => false], ['username' => 'admin']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180926_152030_update_admin_user cannot be reverted.\n";

        return false;
    }
    */
}
