<?php

namespace app\controllers;

use app\filters\IsAdminRule;
use app\filters\UpdateSelfRule;
use app\models\UserEditForm;
use Yii;
use app\models\User;
use app\models\UserSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['@'],
                    ],
                    [
                        'class' => UpdateSelfRule::class,
                        'allow' => true,
                        'actions' => ['update'],
                        'roles' => ['@'],
                        'matchCallback' => function () {
                            return Yii::$app->user->isGuest ||
                                (User::findOne(Yii::$app->user->getId()))->is_admin == false;
                        },
                    ],
                    [
                        'class' => IsAdminRule::class,
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserEditForm();
        $model->setScenario(UserEditForm::SCENARIO_CREATE);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = new User([
                'fio' => $model->fio,
                'is_admin' => $model->is_admin,
                'username' => $model->username,
                'password_hash' => Yii::$app->getSecurity()->generatePasswordHash($model->password),
                'auth_key' => Yii::$app->getSecurity()->generateRandomString(),
            ]);
            if ($user->save())
                return $this->redirect(['view', 'id' => $user->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $user = $this->findModel($id);
        $model = new UserEditForm([
            'fio' => $user->fio,
            'is_admin' => $user->is_admin,
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user->fio = $model->fio;
            $user->is_admin = $model->is_admin;
            if (!empty($model->password)) {
                $user->password_hash = Yii::$app->getSecurity()->generatePasswordHash($model->password);
                $user->auth_key = Yii::$app->getSecurity()->generateRandomString();
            }
            if ($user->save())
                return $this->redirect(['view', 'id' => $user->id]);
        }

//        Yii::debug(print_r($user->errors, true));
//        Yii::debug(print_r($model->errors, true));

        return $this->render('update', [
            'model' => $model,
            'user' => $user,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
